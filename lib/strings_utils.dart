extension StringUtils on String {
  DateTime? stringToDateTime() {
    return DateTime.parse(this);
  }
}