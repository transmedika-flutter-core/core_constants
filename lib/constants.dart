class Constants{
  static const String patternAlphabeticalLowerCase = "(?=.*[a-z])";
  static const String patternAlphabeticalUpperCase = "(?=.*[A-Z])";
  static const String patternNumber = "(?=.*[0-9])";
  static const String patternLengthEight = "(?=.{8,})";
  static const String patternSpecialCharacter = "(?=.*[\$&+,:;=?@#|/'{}\\\\<>.\\[\\]^*()%!-])";
  static const String patternStrengthOne = patternLengthEight+ patternAlphabeticalLowerCase+ patternAlphabeticalUpperCase+ patternNumber+ patternSpecialCharacter;
  static const String patternWeakOne = patternLengthEight+ patternAlphabeticalUpperCase;
  static const String patternWeakTwo = patternLengthEight+ patternAlphabeticalLowerCase;
  static const String patternWeakThree = patternLengthEight+ patternNumber;
  static const String patternWeakFour = patternLengthEight+ patternAlphabeticalUpperCase+  patternAlphabeticalLowerCase;
  static const String patternWeakSix = patternLengthEight+  patternAlphabeticalLowerCase+ patternNumber;
  static const String patternMediumOne = patternLengthEight+ patternAlphabeticalLowerCase+ patternAlphabeticalUpperCase+ patternNumber;
  static const String patternMediumTwo = patternLengthEight+ patternAlphabeticalLowerCase+ patternNumber+  patternSpecialCharacter;
  static const String patternMediumThree = patternLengthEight+ patternAlphabeticalUpperCase+ patternNumber+  patternSpecialCharacter;
  static const int initPaging = 20;
  static const int sizePerPage = 10;
  static const String localeID = "id";
  static const String localeEN = "en";
  static const String localeCountryCodeID = "62";
  static const String localeCountryCodeEN = "44";
  static const String defaultLocale = localeID;
  static const String defaultCountryCode = localeCountryCodeID;
}