import 'package:intl/intl.dart';

extension DateUtils on DateTime {
  String dateTimeToString(String formatForDate, [String? locale]) {
    var formatForText = DateFormat(formatForDate, locale);
    return formatForText.format(this);
  }

  DateTime getStartDateInThisWeek() {
    int daysToMonday = DateTime.monday - this.weekday;
    DateTime startDate = this.add(Duration(days: daysToMonday));
    DateTime newStartDate = DateTime(startDate.year, startDate.month, startDate.day, 00, 00);
    return newStartDate;
  }

  DateTime getEndDateInThisWeek() {
    int daysToSunday = DateTime.sunday - this.weekday;
    DateTime endDate = this.add(Duration(days: daysToSunday));
    DateTime newEndDate = DateTime(endDate.year, endDate.month, endDate.day, 23, 59);
    return newEndDate;
  }
}
