import 'package:flutter/foundation.dart';

enum ImageTypeEnumeration { file, url, localasset}

class ImageType{
  String uuid = UniqueKey().toString();
  ImageTypeEnumeration imageType;
  final String path;

  ImageType(this.imageType, this.path);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ImageType &&
          runtimeType == other.runtimeType &&
          uuid == other.uuid;

  @override
  int get hashCode => uuid.hashCode;
}