import 'package:flutter/cupertino.dart';

enum VideoTypeEnumeration { file, url}

class VideoType{
  String uuid = UniqueKey().toString();
  VideoTypeEnumeration videoType;
  final String path;

  VideoType(this.videoType, this.path);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is VideoType &&
          runtimeType == other.runtimeType &&
          uuid == other.uuid;

  @override
  int get hashCode => uuid.hashCode;
}